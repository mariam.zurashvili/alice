# Alice's Adventures in Wonderland

![Alice's Adventures in Wonderland][1]

## ALICE'S ADVENTURES IN WONDERLAND

### BY LEWIS CARROLL

*WITH FORTY-TWO ILLUSTRATIONS BY JOHN TENNIEL*  
VolumeOne Publishing  
Chicago, Illinois 1998

[A BookVirtual Digital Edition, v1.2][2]
November, 2000
![Alice's Adventures in Wonderland][3]

```md
All in the golden afternoon
  Full leisurely we glide;
For both our oars, with little skill,
  By little arms are plied,
While little hands make vain pretence
  Our wanderings to guide.

Ah, cruel Three! In such an hour,
  Beneath such dreamy weather,
To beg a tale of breath too weak
  To stir the tiniest feather!
Yet what can one poor voice avail
  Against three tongues together?

Imperious Prima flashes forth
  Her edict 'to begin it' –
In gentler tone Secunda hopes
  'There will be nonsense in it!' –
While Tertia interrupts the tale
  Not more than once a minute.

Anon, to sudden silence won,
  In fancy they pursue
The dream-child moving through a land
  Of wonders wild and new,
In friendly chat with bird or beast –
  And half believe it true.

And ever, as the story drained
  The wells of fancy dry,
And faintly strove that weary one
  To put the subject by,
"The rest next time -" "It is next time!"
  The happy voices cry.

Thus grew the tale of Wonderland:
  Thus slowly, one by one,
Its quaint events were hammered out –
  And now the tale is done,
And home we steer, a merry crew,
  Beneath the setting sun.

Alice! a childish story take,
  And with a gentle hand
Lay it were Childhood's dreams are twined
  In Memory's mystic band,
Like pilgrim's wither'd wreath of flowers
  Pluck'd in a far-off land.
```

## Contents

[I. DOWN THE RABBIT-HOLE](#chapter-i)  
[II. THE POOL OF TEARS](#chapter-ii)  
[III. A CAUCUS-RACE AND A LONG TALE](#chapter-iii)  
[IV. THE RABBIT SENDS IN A LITTLE BILL](#chapter-iv)  
[V. ADVICE FROM A CATERPILLAR](#chapter-v)  
[VI. PIG AND PEPPER](#chapter-vi)  
[VII. A MAD TEA-PARTY](#chapter-vii)  
[VIII. THE QUEEN'S CROQUET-GROUND](#chapter-viii)  
[IX. THE MOCK TURTLE'S STORY](#chapter-ix)  
[X. THE LOBSTER QUADRILLE](#chapter-x)  
[XI. WHO STOLE THE TARTS?](#chapter-xi)  
[XII. ALICE'S EVIDENCE](#chapter-xii)  
---  
1

![DOWN THE RABBIT-HOLE][4]
## Chapter I  

DOWN THE RABBIT-HOLE

ALICE was beginning to get very tired of  
sitting by her sister on the bank, and of having  
nothing to do: once or twice she had peeped into  
the book her sister was reading, but it had no  
pictures or conversations in it, “ and what is

---
13

in a game of croquet she was playing against  
herself, for this curious child was very fond of  
pretending to be two people. “ But it ’s no use  
now,” thought poor Alice, “ to pretend to be two  
people ! Why, there ’s hardly enough of me left  
to make *one* respectable person !”  
&nbsp;&nbsp;&nbsp;&nbsp;Soon her eye fell on a little glass box that  
was lying under the table : she opened it, and  
found in it a very small cake, on which the  
words “ EAT ME” were beautifully marked in  
currants. “ Well, I ’ll eat it,” said Alice, “ and if  
it makes me grow larger, I can reach the key ;  
and if it makes me grow smaller, I can creep  
under the door ; so either way I ’ll get into the  
garden, and I don’t care which happens !”  
&nbsp;&nbsp;&nbsp;&nbsp;She ate a little bit, and said anxiously to  
herself “ Which way ? Which way ?” holding her  
hand on the top of her head to feel which way  
it was growing, and she was quite surprised  
to find that she remained the same size : to be  
sure, this is what generally happens when one  
eats cake, but Alice had got so much into the

---
144

“I never heard of ‘Uglification,’ ” Alice ven-  
tured to say. “What is it ?”  
The Gryphon lifted up both its paws in sur-  
prise. “Never heard of uglifying !” it exclaimed.  
“You know what to beautify is, I suppose ?”  
“Yes,” said Alice, doubtfully : “ it means—  
to—make—anything—prettier.”  
“Well then,” the Gryphon went on, “if you  
don ’t know what to uglify is, you *are* a  
simpleton.”  
Alice did not feel encouraged to ask any  
more questions about it, so she turned to the  
Mock Turtle, and said “What else had you to  
learn?”  
“Well, there was Mystery,” the Mock Turtle  
replied, counting off the subjects on his flappers,—  
“Mystery, ancient and modern, with Seaography:  
then Drawling—the Drawling-master was an old  
conger-eel, that used to come once a week: *he*  
taught us Drawling, Stretching, and Fainting in  
Coils.”  
“What *was* that like?” said Alice.  

---
156

  So Alice began telling them her adventures  
from the time when she first saw the White  
Rabbit: she was a little nervous about it just at  
first, the two creatures got so close to her, one  
on each side, and opened their eyes and mouths  
so _very_ wide, but she gained courage as she  
went on. Her listeners were perfectly quiet  
till she got to the part about her repeating  
“_You are old, Father William,_” to the Cater-  
pillar, and the words all coming different, and  
then the Mock Turtle drew a long breath, and  
said, “That’s very curious.”  
  “It’s all about as curious as it can be,” said  
the Gryphon.  
  “It all came different!” the Mock Turtle  
repeated thoughtfully. “I should like to hear  
her try and repeat something now. Tell her  
to begin.” He looked at the Gryphon as if he  
thought it had some kind of authority over  
Alice.  
  “Stand up and repeat ‘’_Tis the voice of the_  
_sluggard,_’” said the Gryphon.

---
182

“Please your Majesty,” said the Knave,  
“I didn’t write it, and they can’t prove I did:  
there ’s no name signed at the end.”  
“If you didn’t sign it,” said the King,“ that  
only makes the matter worse. You must have  
meant some mischief, or else you ’d have signed  
your name like an honest man.”  
There was a general clapping of hands at  
this : it was the first really clever thing the  
King had said that day.  
“That *proves* his guilt,” said the Queen.  
“It proves nothing of the sort !” said  
Alice. “ Why, you don’t even know what they’re  
about !”  
“Read them,” said the King.  
The White Rabbit put on his spectacles.  
“Where shall I begin, please your Majesty ?”  
he asked.  
“Begin at the beginning,” the King said,  
gravely, “ and go on till you come to the end:  
then stop.”  
These were the verses the White Rabbit read: —

[1]: https://pbs.twimg.com/media/EAts-3NXYAQ-qrS.png
[2]: https://www.adobe.com/be_en/active-use/pdf/Alice_in_Wonderland.pdf
[3]: https://www.gutenberg.org/files/19778/19778-h/images/frontipiece.jpg
[4]: https://www.gutenberg.org/files/19778/19778-h/images/p001.png
